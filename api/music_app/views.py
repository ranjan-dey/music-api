from rest_framework import generics
from django.shortcuts import render,get_object_or_404
from rest_framework.permissions import (IsAdminUser,
	AllowAny,
	IsAuthenticated,
	IsAuthenticatedOrReadOnly,)
from .models import Songs
from .serializers import SongSerializer


class ListSongsView(generics.ListAPIView):
    """
    Provides a get method handler.
    """
    queryset = Songs.objects.all()
    serializer_class = SongSerializer

class CreateSongView(generics.CreateAPIView):
    """
    Provides creating new API data
    """
    queryset = Songs.objects.all()
    serializer_class = SongSerializer

    permission_classes =(IsAuthenticated,IsAdminUser)

    def perform_create(self,serializer):
        serializer.save()

class UpdateSongView(generics.RetrieveUpdateAPIView):
    """
    Allows the user to update the music api data
    """
    queryset = Songs.objects.all()
    serializer_class = SongSerializer
    permission_classes = (IsAuthenticated, IsAdminUser)
    lookup_field ="id"

    def get_object(self):
        id = self.kwargs['id']
        return get_object_or_404(Songs, id=id)

    def perform_update(self,serializer):
        serializer.save()


class DeleteSongView(generics.DestroyAPIView):
    queryset = Songs.objects.all()
    serializer_class = SongSerializer
    permission_classes = (IsAuthenticated, IsAdminUser)
    lookup_field ="id"

    def get_object(self):
        id = self.kwargs['id']
        return get_object_or_404(Songs, id=id)

