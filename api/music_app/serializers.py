from .models import Songs
from rest_framework import serializers

class SongSerializer(serializers.ModelSerializer):
    class Meta:
        model = Songs
        fields = ("title" , "artist")
