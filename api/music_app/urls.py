from django.urls import path
from rest_framework_swagger.views import get_swagger_view
from django.conf.urls import url
from .views import (ListSongsView,CreateSongView,UpdateSongView,DeleteSongView, )

schema_view = get_swagger_view(title='Welcome To The Music API')

urlpatterns = [
    url(r'^$', schema_view),
    url(r'^api/songs/', ListSongsView.as_view(), name="songs-all"),
    url(r'^create/',CreateSongView.as_view(), name="create"),
    url(r'^(?P<id>\d+)/edit', UpdateSongView.as_view(), name="edit"),
    url(r'^(?P<id>\d+)/delete', DeleteSongView.as_view(), name="delete"),

]