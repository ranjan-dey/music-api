from django.db import models

# Create your models here.
class Songs(models.Model):
    title = models.CharField(max_length=264)
    artist = models.CharField(max_length=264)


    def __str__(self):
        return "{} -{}".format(self.title,self.artist)